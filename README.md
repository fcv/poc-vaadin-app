# poc-vaadin-app

Simple POC project used to experiment on [Vaadin](https://vaadin.com/). It started as a follow up of [Vaadin's tutorial page](https://vaadin.com/docs/-/part/framework/tutorial.html) and then evolved adpating it to integrate Vaadin and [Spring Boot](https://projects.spring.io/spring-boot/) based on Spring's ["Creating CRUD UI with Vaadin"](https://spring.io/guides/gs/crud-with-vaadin/) getting started document.

# Getting Started

This projects requires Java 8 and it's built using [Maven 3](http://maven.apache.org/). So it can be packaged using command:

    $ mvn package

It is developed over Spring Boot so one option to run it is using Spring Boot's maven plugin command:

    $ mvn spring-boot:run

This command will start an embedded Tomcat instance and deploy project in it. After system is up and running it may be accessed through URL [http://localhost:8080/](http://localhost:8080/).

**Note**: Since integration with Spring Boot Jetty's maven plugin (`org.eclipse.jetty:jetty-maven-plugin`) no longer works leading to exceptions during starting up. 