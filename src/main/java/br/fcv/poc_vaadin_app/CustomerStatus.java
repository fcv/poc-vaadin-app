package br.fcv.poc_vaadin_app;

public enum CustomerStatus {
	IMPORTED_LEAD,
	NOT_CONTACTED,
	CONTACTED,
	CUSTOMER,
	CLOSED_LOST
}