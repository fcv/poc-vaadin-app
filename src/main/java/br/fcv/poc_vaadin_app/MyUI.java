package br.fcv.poc_vaadin_app;

import com.google.common.collect.FluentIterable;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.data.Container.Indexed;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.event.FieldEvents.TextChangeEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.Button;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.annotation.WebServlet;
import java.util.Optional;
import java.util.Set;

import static com.google.common.base.Strings.isNullOrEmpty;
import static com.google.common.collect.Lists.newArrayList;
import static com.vaadin.ui.themes.ValoTheme.LAYOUT_COMPONENT_GROUP;

/**
 * This UI is the application entry point. A UI may either represent a browser window
 * (or tab) or some part of a html page where a Vaadin application is embedded.
 * <p>
 * The UI is initialized using {@link #init(VaadinRequest)}. This method is intended to be
 * overridden to add component to the user interface and initialize non-component functionality.
 */
@SpringUI
@Theme("mytheme")
public class MyUI extends UI {

	private final CustomerRepository repository;
	private final CustomerForm form;

	private final TextField filterText = new TextField();
	private final Grid grid = new Grid();

	@Autowired
	public MyUI(CustomerRepository repository, CustomerForm form) {
		this.repository = repository;
		this.form = form;
	}

	@Override
	protected void init(VaadinRequest vaadinRequest) {
		final VerticalLayout layout = new VerticalLayout();

		filterText.setInputPrompt("filter by name...");
		filterText.addTextChangeListener(this::filterByNameFragment);

		Button clearFilterText = new Button(FontAwesome.TIMES);
		clearFilterText.setDescription("Clear the current filter");
		clearFilterText.addClickListener(e -> {
			filterText.clear();
			updateCustomerList();
		});

		CssLayout filtering = new CssLayout();
		filtering.addComponents(filterText, clearFilterText);
		filtering.setStyleName(LAYOUT_COMPONENT_GROUP);

		Button addCustomer = new Button("Add new customer", event -> {
			grid.select(null);
			form.setCustomer(new Customer());
			form.setVisible(true);
		});
		HorizontalLayout toolbar = new HorizontalLayout(filtering, addCustomer);

		// form is set to not visible on page load
		form.setVisible(false);
		form.setChangeHandler(() -> {
			form.setVisible(false);
			updateCustomerList();
		});

		grid.addSelectionListener(evt -> {
			Set<Object> selected = evt.getSelected();
			if (selected.isEmpty()) {
				form.setVisible(false);
			} else {
				Customer customer = (Customer) selected.iterator().next();
				form.setCustomer(customer);
				form.setVisible(true);
			}
		});

		HorizontalLayout main = new HorizontalLayout(grid, form);
		main.setSpacing(true);
		main.setSizeFull();
		grid.setSizeFull();
		main.setExpandRatio(grid, 1);

		updateCustomerList();

		layout.addComponents(toolbar, main);
		layout.setMargin(true);
		layout.setSpacing(true);

		setContent(layout);
	}

	public void updateCustomerList() {
		filterByNameFragment(filterText.getValue());
	}

	public void filterByNameFragment(TextChangeEvent evt) {
		filterByNameFragment(evt.getText());
	}

	public void filterByNameFragment(String nameFragment) {
		filterByNameFragment(isNullOrEmpty(nameFragment) ? Optional.empty() : Optional.of(nameFragment));
	}

	public void filterByNameFragment(Optional<String> nameFragment) {

		Iterable<Customer> customers = nameFragment
				.map(repository::findByFirstNameAndLastNameContainsIgnoreCase)
				.orElseGet(repository::findAll);

		// TODO: consider using an implementation that does not load all results into memory
		Indexed container = new BeanItemContainer<>(Customer.class, newArrayList(customers));
		grid.setContainerDataSource(container);
	}

	@WebServlet(urlPatterns = "/*", name = "MyUIServlet", asyncSupported = true)
	@VaadinServletConfiguration(ui = MyUI.class, productionMode = false)
	public static class MyUIServlet extends VaadinServlet {
	}
}
