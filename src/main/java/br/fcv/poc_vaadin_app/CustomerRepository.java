package br.fcv.poc_vaadin_app;

import br.fcv.poc_vaadin_app.support.CustomerRepositoryCustom;
import org.springframework.data.repository.CrudRepository;

public interface CustomerRepository extends CrudRepository<Customer, Long>, CustomerRepositoryCustom {

}
