package br.fcv.poc_vaadin_app;

import org.slf4j.Logger;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.Calendar;
import java.util.Random;
import java.util.stream.Stream;

import static java.time.ZoneId.systemDefault;
import static java.util.stream.Collectors.toList;

@Component
public class SampleDataCLR implements CommandLineRunner {

	private final Logger logger;
	private final CustomerRepository repository;

	public SampleDataCLR(Logger logger, CustomerRepository repository) {
		this.logger = logger;
		this.repository = repository;
	}

	@Override
	public void run(String... args) throws Exception {
		Random r = new Random(0);

		Iterable<Customer> customers = Stream.of("Gabrielle Patel", "Brian Robinson", "Eduardo Haugen",
				"Koen Johansen", "Alejandro Macdonald", "Angel Karlsson", "Yahir Gustavsson", "Haiden Svensson",
				"Emily Stewart", "Corinne Davis", "Ryann Davis", "Yurem Jackson", "Kelly Gustavsson",
				"Eileen Walker", "Katelyn Martin", "Israel Carlsson", "Quinn Hansson", "Makena Smith",
				"Danielle Watson", "Leland Harris", "Gunner Karlsen", "Jamar Olsson", "Lara Martin",
				"Ann Andersson", "Remington Andersson", "Rene Carlsson", "Elvis Olsen", "Solomon Olsen",
				"Jaydan Jackson", "Bernard Nilsen")
				.map(name -> {
					String[] split = name.split(" ");
					Customer c = new Customer();
					c.setFirstName(split[0]);
					c.setLastName(split[1]);
					c.setEmail(split[0].toLowerCase() + "@" + split[1].toLowerCase() + ".com");
					c.setStatus(CustomerStatus.values()[r.nextInt(CustomerStatus.values().length)]);
					Calendar cal = Calendar.getInstance();
					int daysOld = 0 - r.nextInt(365 * 15 + 365 * 60);
					cal.add(Calendar.DAY_OF_MONTH, daysOld);
					c.setBirthDate(cal.toInstant().atZone(systemDefault()).toLocalDate());
					return c;
				})
				.collect(toList());

		customers = repository.save(customers);
		logger.info("Stored customers: {}", customers);
	}

}
