package br.fcv.poc_vaadin_app;

import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.NativeSelect;
import com.vaadin.ui.PopupDateField;
import com.vaadin.ui.TextField;

import static com.vaadin.event.ShortcutAction.KeyCode.ENTER;
import static com.vaadin.ui.themes.ValoTheme.BUTTON_PRIMARY;
import static java.util.Arrays.asList;

@SpringComponent
@UIScope
public class CustomerForm extends FormLayout {

	private static final long serialVersionUID = 7933841522150066419L;

	// form fields
	private final TextField firstName = new TextField("First name");
	private final TextField lastName = new TextField("Last name");
	private final TextField email = new TextField("Email");
	private final NativeSelect status = new NativeSelect("Status");
	private final PopupDateField birthdate = new PopupDateField("Birthday");
	private final Button save = new Button("Save", this::save);
	private final Button delete = new Button("Delete", this::delete);

	private final CustomerRepository repository;

	// Customer instance being edit
	private Customer customer;
	private ChangeHandler changeHandler;


	public CustomerForm(CustomerRepository repository) {
		this.repository = repository;

		status.addItems(asList(CustomerStatus.values()));

		save.setStyleName(BUTTON_PRIMARY);
		save.setClickShortcut(ENTER);

		birthdate.setConverter(new DateToLocalDateConverter());

		setSizeUndefined();
		HorizontalLayout buttons = new HorizontalLayout(save, delete);
		buttons.setSpacing(true);
		addComponents(firstName, lastName, email, status, birthdate, buttons);
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
		BeanFieldGroup.bindFieldsUnbuffered(customer, this);

		delete.setVisible(customer.isPersisted());
		firstName.selectAll();
	}

	public void setChangeHandler(ChangeHandler changeHandler) {
		this.changeHandler = changeHandler;
	}

	private void delete(ClickEvent evt) {
		repository.delete(customer);
		if (changeHandler != null) {
			changeHandler.onChange();
		}
	}

	private void save(ClickEvent evt) {
		customer = repository.save(customer);
		if (changeHandler != null) {
			changeHandler.onChange();
		}
	}

	public static interface ChangeHandler {
		void onChange();
	}
}
