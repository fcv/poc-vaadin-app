package br.fcv.poc_vaadin_app.support;

import br.fcv.poc_vaadin_app.Customer;
import org.slf4j.Logger;
import org.springframework.data.repository.NoRepositoryBean;

import javax.persistence.EntityManager;

import static java.util.Objects.requireNonNull;

@NoRepositoryBean
public class CustomerRepositoryImpl implements CustomerRepositoryCustom {

	private final Logger logger;
	private final EntityManager em;


	public CustomerRepositoryImpl(Logger logger, EntityManager em) {
		this.logger = logger;
		this.em = em;
	}

	@Override
	public Iterable<Customer> findByFirstNameAndLastNameContainsIgnoreCase(String nameFragment) {

		logger.debug("findByFirstNameAndLastNameContainsIgnoreCase(nameFragment: {})", nameFragment);

		requireNonNull(nameFragment, "nameFragment cannot be null");
		// TODO: consider escaping LIKE special characters
		nameFragment = '%' + nameFragment + '%';

		return em.createQuery("select c from Customer c where upper(c.firstName) like upper(:nameFragment) or upper(c.lastName) like upper(:nameFragment)", Customer.class)
				.setParameter("nameFragment", nameFragment)
				.getResultList();
	}
}
