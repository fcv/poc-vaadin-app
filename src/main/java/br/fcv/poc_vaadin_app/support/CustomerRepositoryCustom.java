package br.fcv.poc_vaadin_app.support;

import br.fcv.poc_vaadin_app.Customer;

public interface CustomerRepositoryCustom {

	public Iterable<Customer> findByFirstNameAndLastNameContainsIgnoreCase(String nameFragment);

}
