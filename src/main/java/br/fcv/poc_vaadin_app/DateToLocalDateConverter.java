package br.fcv.poc_vaadin_app;

import com.vaadin.data.util.converter.Converter;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.Locale;

/**
 * Converts {@code java.util.Date} in Java 8's {@code java.time.LocalDate}.
 *
 * Based on http://stackoverflow.com/a/37236637
 */
public class DateToLocalDateConverter implements Converter<Date, LocalDate> {

	@Override
	public LocalDate convertToModel(Date value, Class<? extends LocalDate> targetType, Locale locale) throws ConversionException {
		LocalDate modelValue = null;
		if (value != null) {
			modelValue = value.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		}
		return modelValue;
	}

	@Override
	public Date convertToPresentation(LocalDate value, Class<? extends Date> targetType, Locale locale) throws ConversionException {
		Date presentationValue = null;
		if (value != null) {
			presentationValue = Date.from(value.atStartOfDay(ZoneId.systemDefault()).toInstant());
		}
		return presentationValue;
	}

	@Override
	public Class<LocalDate> getModelType() {
		return LocalDate.class;
	}

	@Override
	public Class<Date> getPresentationType() {
		return Date.class;
	}
}
